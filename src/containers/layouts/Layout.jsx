import React, {Component} from 'react';
import Aux from '../../hoc/Aux';

import { connect } from 'react-redux'

import 'bootstrap/dist/css/bootstrap.min.css';
import './Layout.css';
import SideBar from '../components/sidebar/SideBar';
import Content from '../components/content/Content';
import Login from '../../LoginForm/Login'

class Layout extends Component{

  constructor(props) {
    super(props);
    this.state = {
      isOpen: true
    }
  }

  toggle = () => {
    this.setState({isOpen: !this.state.isOpen});
  }

  render(){
    // const { authentication } = this.props
    return(

        <div className="App wrapper">
          <SideBar toggle={this.toggle} isOpen={this.state.isOpen}/>
          <Content toggle={this.toggle} isOpen={this.state.isOpen}>
            <main>{this.props.children}</main>
          </Content> 
          <Login/> 
        </div>
    );
  }
} 

const mapStateToProps = ({  }) => ({  })

const mapDispatchToProps = dispatch => ({
  handleOnLogout () {
    // dispatch(logout())
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Layout)



