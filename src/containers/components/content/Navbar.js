import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAlignLeft } from '@fortawesome/free-solid-svg-icons';
import { Navbar, Button, NavbarToggler, Collapse, Nav, NavItem, NavLink } from 'reactstrap';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { connect } from 'react-redux'
import * as userActions from '../../../store/actions/userActions'
import { bindActionCreators } from 'redux'

import NewQuestion from '../../../Question/New'

class NavBar extends React.Component {

  constructor(props) {
    super(props);

   this.state = {
     isOpen: false,
     modal: false,
     isOpenQuestion: false
   };

   this.toggle = this.toggle.bind(this);
   this.openModal = this.openModal.bind(this);
   this.logout = this.logout.bind(this);
   this.openModalQuestion = this.openModalQuestion.bind(this);

 }

 toggle() {
   this.setState({
     isOpen: !this.state.isOpen  // navbar collapse
   });
 }

  openModal() {
    this.props.actions.openLoginModal()
  }

  logout(){
    this.props.actions.logout()
  }

  openModalQuestion(){
    if(this.props.auth.loggedIn){
      this.setState({
        isOpenQuestion: !this.state.isOpenQuestion  
      });
    }
    else{
      this.openModal()
    }
   
  }

  render(){
    const {auth} = this.props
    let content = ''
    if(auth.loggedIn){
      content = (
        <div className="row">
          <div className="col-md-6">
            {auth.user.user.email}
          </div>
          <div className="col-md-6">
            <Button color="danger" onClick={this.logout}>Logout</Button>
          </div>
          
        </div>
      );
    }
    else{
      content = (
          <Button color="danger" onClick={this.openModal}>Login</Button>
      );
    }
    return (
      <div>
        <Navbar color="light" light className="navbar shadow-sm p-3 mb-5 bg-white rounded" expand="md">
          <Button color="info" onClick={this.props.toggle}>
            <FontAwesomeIcon icon={faAlignLeft}/>
          </Button>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            
            <NavItem>
              <NavLink href="#">
                  <Button color="success" onClick={this.openModalQuestion}>Nueva Pregunta</Button>
                </NavLink> 
            </NavItem> 
            <NavItem>
                <NavLink href="#">
                  {content}
                </NavLink> 

            </NavItem>
          </Nav>
        </Collapse>

        </Navbar>

        <NewQuestion show={this.state.isOpenQuestion} openModal={this.openModalQuestion}/>

      </div>
      
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.authentication,
  }
}

function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar)
