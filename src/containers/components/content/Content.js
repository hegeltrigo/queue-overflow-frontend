import React from 'react';
import classNames from 'classnames';
import { Container } from 'reactstrap';
import NavBar from './Navbar';
import Aux from '../../../hoc/Aux';

class Content extends React.Component {

  render() {
    return (
      <Aux>
        <Container fluid className={classNames('content', {'is-open': this.props.isOpen})}>
          <NavBar toggle={this.props.toggle}/>
          <main>{this.props.children}</main>
        </Container>
      </Aux>  
    );
  }
}

export default Content;
