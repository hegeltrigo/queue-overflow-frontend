import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as answerActions from '../store/actions/answerActions'
import { bindActionCreators } from 'redux'

export class AnswerForm extends Component {

  constructor (props){
    super(props);
    this.state = {
      token: '',
      form: {
      }
    }
  }
 

  handleSubmit = event => {
    event.preventDefault()
    console.log(this.state)
    
    this.props.actions.createAnswer(this.state.form)


  }

  componentDidMount = () => {
    this.setState({
      form: { ...this.state.form, question_id: parseInt(this.props.questionId) }
    })  
    console.log(this.state)
  }


    render() {
        return (
            <div>
                <form action="" onSubmit={this.handleSubmit}>
                      <div>
                          
                          <textarea
                            rows="7"
                            type='text'
                            name='content'
                            placeholder="Respuesta"
                            className="form-control"

                            onChange={ev => {
                                this.setState({
                                form: { ...this.state.form, content: ev.target.value }
                                })
                            }}
                          />
                      </div>
                    <br/>
                    <input type="submit" className="btn btn-success" value="Responder" onChange={ev => {
                       
                    }}/>
                </form> 
            </div>
        )
    }
}

function mapStateToProps(state) {
  return {
    auth: state.authentication,
    question: state.QuestionReducer.question
  }
}

function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(answerActions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AnswerForm)
