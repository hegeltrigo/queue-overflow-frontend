import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import * as questionActions from '../store/actions/questionActions'
import { bindActionCreators } from 'redux'
import { Button, Fade } from 'reactstrap';

import {Link} from 'react-router-dom';

export class Home extends Component {

    constructor(props) {
        super(props);
        this.state = { fadeIn: true };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            fadeIn: !this.state.fadeIn
        });
    }

    componentDidMount() {
        this.props.actions.GetAllQuestions()
      }
    render() {

        const listItems = this.props.questions.map((question) =>
            <div>
                 <Card>
                    <CardBody>
                        <CardTitle>
                            <Link to ={`/show/${question.id}`} >{question.title}</Link>
                        </CardTitle>
                        {/* <CardSubtitle>Card subtitle</CardSubtitle> */}
                        <CardText>{question.content}</CardText>
                        {/* <CardText>{question.question_date}</CardText> */}
                        {/* <Button color="primary">Responder</Button>{' '} */}
                        {/* <Button color="success">Comentar</Button> */}


                        
                        
                    </CardBody>
                </Card>    
                {/* <Card>
                    <CardBody>
                        <CardTitle>Respuestas</CardTitle>
                    </CardBody>
                </Card>           */}
                <br></br>    
            </div>
           
            );

        return (
            <div>
                {listItems}  
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        questions: state.QuestionReducer.questions,
        loading: state.QuestionReducer.questions
    }
  }
  
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(questionActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
