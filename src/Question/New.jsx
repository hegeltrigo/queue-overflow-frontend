import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, ModalHeader, ModalBody, Button, ModalFooter } from 'reactstrap';
import * as questionActions from '../store/actions/questionActions'
import { bindActionCreators } from 'redux'


export class New extends Component {
    
  state = {
    token: '',
    form: {}
  }

  handleSubmit = async event => {
    event.preventDefault()
    try {
      let res = await this.props.actions.createQuestion(this.state.form)
      this.props.openModal()
      console.log('esto es la respuesta', res)
    } catch(error) {
      // do something with error
    }
    
  }
  
  render() {
      return (
          <div>

            <Modal isOpen={this.props.show} toggle={this.props.openModal}>
                <ModalHeader toggle={this.props.openModal}>Nueva Pregunta</ModalHeader>
                <ModalBody>
                <form onSubmit={this.handleSubmit}>
                  <div>
                  
                  <input
                  type='text'
                  name='email'
                  placeholder="Titulo"
                  className="form-control"

                  onChange={ev => {
                      this.setState({
                      form: { ...this.state.form, title: ev.target.value }
                      })
                  }}
                  />
              </div>
              
              <br></br>
              <div>
                  
                  <textarea
                  name='content'
                  rows="10"
                  placeholder="contenido"
                  className="form-control"

                  onChange={ev => {
                      this.setState({
                      form: { ...this.state.form, content: ev.target.value }
                      })
                  }}
                  />
              </div>

              <br></br>


              <Button buttonType="Success" clicked={this.handleSubmit}>Guardar</Button>
              {/* <div> {authentication.loading && 'Cargando...'} </div> */}
              </form>                  
              </ModalBody>
                
            </Modal>
                
              
          </div>
      )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.authentication,
  }
}

function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(questionActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(New)
