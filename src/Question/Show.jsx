import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as questionActions from '../store/actions/questionActions'
import { bindActionCreators } from 'redux'
import AnswerForm from './../Answer/AnswerForm'
import { thisExpression } from '@babel/types';
import { Alert } from 'reactstrap';
import t from 'typy';


export class Show extends Component {

    componentDidMount(){
        this.props.actions.getQuestion(this.props.match.params.id)
    }
    render() {
        const {question, auth} = this.props
        let answers = []
        if(question.answers != null){
          answers = question.answers
        }
        let answerQuestion = ''


        if(auth.loggedIn){
          answerQuestion = (
            <div>
                <AnswerForm questionId={this.props.match.params.id}/>
                <br></br>
            </div>
          );      
           
        }

        const listAnswers = answers.map((answer) =>
             <Alert color="info">
              <p>{answer.content}</p>
              </Alert>
        );
        
        return (
            <div>
              <Alert color="success">
                <div className="row">
                    <div className="col-md-12">
                        <h1>{question.title}</h1>
                    </div>
                    <div className="col-md-12">
                        <p>{question.content}</p>
                    </div>
                </div>  
              </Alert>
              
              <div className="row">
                  <div className="col-md-12">
                    <h6>Respuestas</h6>
                    {listAnswers}
                  </div>
                 
              </div>      
              <div className="row">
                  <div className="col-md-12">
                   {answerQuestion}
                  </div>
                 
              </div>    
                
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      auth: state.authentication,
      question: state.QuestionReducer.question
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(questionActions, dispatch)
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Show)
