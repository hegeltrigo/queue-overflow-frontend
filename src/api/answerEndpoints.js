import {HttpClient} from './httpClient' 
import { authHeader } from '../helpers'

// This is the API. The backend root URL can be set from here.
const API = 'http://127.0.0.1:8000/api'

//Setting the todos URI
const ANSWER_API = `${API}/answers`

// The CRUD Operations of the Todo Resource.


//Create
const createAnswer = answer => {
    return HttpClient.post(ANSWER_API, answer, { headers: authHeader() })
}

//Read
const getAnswer = (id) => {
    return HttpClient.get(`${ANSWER_API}/${id}`, { })
}

const getAllAnswersForQuestion = (question_id) => {
  return HttpClient.get(ANSWER_API)
}

// const getAllMyVideos = () => {
//   return HttpClient.get(`${ANSWER_API}`, { headers: authHeader() })

// }

// //Update
// const updateVideo = video => {
//     return HttpClient.put(`${VIDEOS_API}/${video.id}`, video, { headers: authHeader() })
// }

// //Delete
// const removeVideo = id => {
//     return HttpClient.delete(`${VIDEOS_API}/${id}`, { headers: authHeader() })
// }

//Encapsulating in a JSON object

// const TodoApi = {createTodo, getTodo, updateTodo, removeTodo}
const AnswerEndpoints = { createAnswer, getAnswer }
export {AnswerEndpoints}