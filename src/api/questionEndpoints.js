import {HttpClient} from './httpClient' 
import { authHeader } from '../helpers'

// This is the API. The backend root URL can be set from here.
const API = 'http://127.0.0.1:8000/api'

//Setting the todos URI
const QUESTION_API = `${API}/questions`

// The CRUD Operations of the Todo Resource.


//Create
const createQuestion = question => {
    return HttpClient.post(QUESTION_API, question, { headers: authHeader() })
}

//Read
const getQuestion = (id) => {
    return HttpClient.get(`${QUESTION_API}/${id}`, { })
}

const getAllQuestions = () => {
  return HttpClient.get(QUESTION_API)
}

// const getAllMyVideos = () => {
//   return HttpClient.get(`${QUESTION_API}`, { headers: authHeader() })

// }

// //Update
// const updateVideo = video => {
//     return HttpClient.put(`${VIDEOS_API}/${video.id}`, video, { headers: authHeader() })
// }

// //Delete
// const removeVideo = id => {
//     return HttpClient.delete(`${VIDEOS_API}/${id}`, { headers: authHeader() })
// }

//Encapsulating in a JSON object

// const TodoApi = {createTodo, getTodo, updateTodo, removeTodo}
const QuestionEndpoints = { getAllQuestions, createQuestion, getQuestion }
export {QuestionEndpoints}