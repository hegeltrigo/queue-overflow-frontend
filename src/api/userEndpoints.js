import { HttpClient } from './httpClient' 
import { authHeader } from '../helpers'

// This is the API. The backend root URL can be set from here.
const API = 'http://127.0.0.1:8000/api/auth'

//Setting the todos URI
const USERS_API = `${API}`

  // let user = JSON.parse(localStorage.getItem('user'));

// Authentication
const login = (email, password) => {
  return HttpClient.post(`${USERS_API}/login`, {email: email, password: password} )
}

const logout = () => {
  return HttpClient.post(`${USERS_API}/logout`, {}, {headers: authHeader()})
}

const UserEndpoints = { login, logout }

export { UserEndpoints }