import React, { Component } from 'react';
import {Route} from 'react-router-dom'
import Layout from './containers/layouts/Layout'
// import Login from './Login/Login'
import Home from './Home/Home'
import Show from './Question/Show'

class App extends Component {

  render() {
    return (
      <div>
         <Layout>
           <Route path="/" exact component={Home}/>
           <Route path="/show/:id" exact component={Show}/>

           {/* <Route path="/MyVideos" component={MyVideos}/>
           <Route path="/Logout" component={Logout}/>
           <Route path="/ShowVideo" component={ShowVideo}/>  */}


           {/* <Route path="/Login" component={Login}/> */}

         </Layout>
      </div>
    );
  }
}

export default App;
