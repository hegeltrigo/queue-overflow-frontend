import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginForm from './LoginForm'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import * as userActions from '../store/actions/userActions'
import { bindActionCreators } from 'redux'

export class Login extends Component {

  constructor(props) {
    super(props);

   this.state = {
    //  isOpen: false,
     modal: true
   };

  //  this.toggle = this.toggle.bind(this);
   this.openModal = this.openModal.bind(this);
  }

  // toggle() {
  //   this.setState({
  //     isOpen: !this.state.isOpen  
  //   });
  // }

  openModal() {
    this.props.actions.openLoginModal()
  }

  render() {
      const {auth} = this.props
      return (
          <div>
              <Modal isOpen={auth.showLogin} toggle={this.openModal}>
                  <ModalHeader toggle={this.openModal}>Login</ModalHeader>
                  <ModalBody>
                    <LoginForm/>
                  </ModalBody>
                  {/* <ModalFooter>
                      <Button color="primary" onClick={this.openModal}>Do Something</Button>{' '}
                      <Button color="secondary" onClick={this.openModal}>Cancel</Button>
                  </ModalFooter> */}
              </Modal>
          </div>
      )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.authentication,
  }
}

function mapDispatchToProps(dispatch) {
  return {
      actions: bindActionCreators(userActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
