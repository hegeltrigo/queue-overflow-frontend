import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Navbar, Button, NavbarToggler, Collapse, Nav, NavItem, NavLink } from 'reactstrap';

import * as userActions from '../store/actions/userActions'
import { bindActionCreators } from 'redux'

export class LoginForm extends Component {
    
    state = {
      token: '',
      form: {}
    }

    handleSubmit = event => {
      event.preventDefault()
    //   console.log('aqui tiene que loguearse')
      // const { handleOnLogin } = this.props
      // handleOnLogin(this.state.form)
      this.props.actions.login(this.state.form)

    }

    render() {
        return (
            <div>
              <form onSubmit={this.handleSubmit}>
                <div>
                    
                    <input
                    type='text'
                    name='email'
                    placeholder="Email"
                    className="form-control"

                    onChange={ev => {
                        this.setState({
                        form: { ...this.state.form, email: ev.target.value }
                        })
                    }}
                    />
                </div>
                <br></br>
                <div>
                    
                    <input
                    type='password'
                    name='password'
                    placeholder="Password"
                    className="form-control"
                    onChange={ev => {
                        this.setState({
                        form: {
                            ...this.state.form,
                            password: ev.target.value
                        }
                        })
                    }}
                    />
                </div>
                <br></br>
                <Button buttonType="Success" clicked={this.handleSubmit}>Login</Button>
                <div> {this.props.auth.loading && 'Cargando...'} </div>
                </form>
                
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      auth: state.authentication,
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(userActions, dispatch)
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)
