import { UserEndpoints } from "../../api/userEndpoints";
import { userTypes } from '../types'

export const login = (user) => {
  return (dispatch) => {
      dispatch(loginRequest())
      console.log("esto es lo que llega", user)

      UserEndpoints.login(user.email, user.password).then(res => {
          let data = res.data;
          localStorage.setItem('user', JSON.stringify(data));
          return dispatch(loginSuccess(res));
      }).catch((e) => {
        return dispatch(loginFailure(e));
      })
  }
}

const loginRequest = () => { return { type: userTypes.LOGIN_REQUEST }}
const loginSuccess = (response) => { return { type: userTypes.LOGIN_SUCCESS, response } }
const loginFailure = (errorMessage)=> { return { type: userTypes.LOGIN_FAILURE, errorMessage } }

export const logout = () => {
  return (dispatch) => {
    UserEndpoints.logout().then(res => {
      console.log('ESTO RESPONDIO EL SERVIDOR CON LOGOUT: ', res)
      localStorage.removeItem('user');
      return dispatch(logoutSuccess());
    }).catch((e) => {
      console.log(e);
    })
  }
}

const logoutSuccess = () => { return { type: userTypes.LOGOUT } }


export const openLoginModal = () => { return { type: userTypes.OPEN_LOGIN_MODAL }}
export const closeLoginModal = () => { return { type: userTypes.CLOSE_LOGIN_MODAL }}
