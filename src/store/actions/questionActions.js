import { QuestionEndpoints } from "../../api/questionEndpoints";
import { questionTypes } from '../types'

export const GetAllQuestions = () => {
    return (dispatch) => {
        dispatch(GetAllQuestionsLoading())
        QuestionEndpoints.getAllQuestions().then(res => {
            console.log('entro en items')
            return dispatch(GetAllQuestionsSuccess(res.data));
        }).catch((e) => {
          return dispatch(GetAllQuestionsFailure(e));
        })
    }
}

const GetAllQuestionsSuccess = (questions) => {return { type: questionTypes.GET_ALL_QUESTIONS_SUCCESS, questions }}
const GetAllQuestionsLoading = () =>{ return { type: questionTypes.GET_ALL_QUESTIONS_LOADING, loading: true } }
const GetAllQuestionsFailure = (errorMessage)=> { return { type: questionTypes.GET_ALL_QUESTIONS_FAILURE, errorMessage } }


export const createQuestion = (question) => {
    return (dispatch) => {
      dispatch(createQuestionRequest())
      QuestionEndpoints.createQuestion(question).then(res => {
          let data = res.data;

          return dispatch(createQuestionSuccess(res));
      }).catch((e) => {
        return dispatch(createQuestionFailure(e));
      })
    }
  }
  
  const createQuestionRequest = () => { return { type: questionTypes.CREATE_QUESTION_REQUEST }}
  const createQuestionSuccess = (response) => { return { type: questionTypes.CREATE_QUESTION_SUCCESS, response }}
  const createQuestionFailure = (errorMessage) => { return { type: questionTypes.CREATE_QUESTION_FAILURE, errorMessage }}
  

  export const getQuestion = (id) => {
    return (dispatch) => {
      dispatch(getQuestionRequest())
      QuestionEndpoints.getQuestion(id).then(res => {
          return dispatch(getQuestionSuccess(res));
      }).catch((e) => {
        return dispatch(getQuestionFailure(e));
      })
    }
  }
  
  const getQuestionRequest = () => { return { type: questionTypes.GET_QUESTION_REQUEST }}
  const getQuestionSuccess = (response) => { return { type: questionTypes.GET_QUESTION_SUCCESS, response }}
  const getQuestionFailure = (errorMessage) => { return { type: questionTypes.GET_QUESTION_FAILURE, errorMessage }}
  