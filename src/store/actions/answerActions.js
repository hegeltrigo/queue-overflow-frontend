import { AnswerEndpoints } from "../../api/answerEndpoints";
import { answerTypes } from '../types'


export const createAnswer = (answer) => {
    return (dispatch) => {
      dispatch(createAnswerRequest())
      AnswerEndpoints.createAnswer(answer).then(res => {
          let data = res.data;

          return dispatch(createAnswerSuccess(res));
      }).catch((e) => {
        return dispatch(createAnswerFailure(e));
      })
    }
  }
  
  const createAnswerRequest = () => { return { type: answerTypes.CREATE_ANSWER_REQUEST }}
  const createAnswerSuccess = (response) => { return { type: answerTypes.CREATE_ANSWER_SUCCESS, response }}
  const createAnswerFailure = (errorMessage) => { return { type: answerTypes.CREATE_ANSWER_FAILURE, errorMessage }}
  