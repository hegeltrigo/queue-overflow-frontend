import { questionTypes } from '../types'
import { answerTypes } from '../types'


const initialStateQuestions = {
  questions: [],
  question: {},
  loading: true,
  errorMessage: ''
}

export const QuestionReducer = (state = initialStateQuestions, action) => {
    switch(action.type){

      case questionTypes.GET_ALL_QUESTIONS_SUCCESS:
        return {
          ...state,
          questions: action.questions,
          loading: false
        }
     case questionTypes.GET_ALL_QUESTIONS_LOADING:
        return {
            ...state,
            loading: true
        }
      
      case questionTypes.GET_ALL_QUESTIONS_FAILURE:
        return{
          ...state,
          loading: false,
          errorMessage: action.errorMessage

        }

      case questionTypes.CREATE_QUESTION_SUCCESS:
          return {
            ...state,
            questions: [action.response.data, ...state.questions],
            loading: false
          }
      case questionTypes.CREATE_QUESTION_REQUEST:
          return {
              ...state,
              loading: true
          }
      case questionTypes.CREATE_QUESTION_FAILURE:
          return{
            ...state,
            loading: false,
            errorMessage: action.errorMessage
          }  
      case questionTypes.GET_QUESTION_SUCCESS:
        return {
          ...state,
          question: action.response.data,
          loading: false
        }
      case questionTypes.GET_QUESTION_REQUEST:
        return {
            ...state,
            loading: true
        }
          
      case questionTypes.GET_QUESTION_FAILURE:
        return{
          ...state,
          loading: false,
          errorMessage: action.errorMessage

        } 

      case answerTypes.CREATE_ANSWER_REQUEST:
          return {
              ...state,
              loading: true
          }

      case answerTypes.CREATE_ANSWER_FAILURE:
          return{
            ...state,
            loading: false,
            errorMessage: action.errorMessage
          }  

      case answerTypes.CREATE_ANSWER_SUCCESS:

        let new_question = state.question
        new_question.answers = state.question.answers
        new_question.answers.push(action.response.data)

        console.log('esto es lo que tiene new questions', new_question.answers)
        return {
          ...state,
          question: new_question,
          loading: false
        }  
        
      default: 
        return state; 
    }
}